import React, { Component } from 'react'
import image9 from '../images/image9.JPG'
import image3 from '../images/image3.JPG'
import image4 from '../images/image4.JPG'

 class HeaderSection extends Component {
    render() {
        return (
            <div className="header">
            <div className="header_one">
                <div>
                    <p className="header_time">4.02</p>
                </div>
                <div>
                    <span className='image8'>
                        <img src={image9} className='' alt='battery' />
                    </span>
                </div>
            </div> 

                <div className="header_two">
                    <div className="logo_border">
                    <div className='logo'>
                       <h3>LOGO</h3>
                    </div>
                    </div>
                    
                    <div>
                    <span className='image3'>
                        <img src={image3} className='' alt='logo' />
                    </span>
                    <span className='image4'>
                        <img src={image4} className='' alt='battery' />
                    </span>
                    </div>
                </div>
             </div>   
        )
    }
}

export default HeaderSection