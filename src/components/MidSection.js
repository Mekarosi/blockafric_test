import React, { Component } from 'react'

class MidSection extends Component {
  render() {
    return (
      <div>
        <div className="mid_sectionA">
          <h3 className="mid_sectionA_text">WALLET BALANCE</h3>
          <h3 className="mid_sectionA_text">TOKEN BALANCE:</h3>
          <h3 className="mid_sectionA_text">NAIRA BALANCE:</h3>

        </div>
        <div className="mid_sectionB">
          <div className="mid_sectionB_one">
            <p className="mid_sectionB_one_text">Select Currency:</p>
          </div>

        </div>

        <div className='token_section'>
          <h3 className='token_text'> <span className="token_text_one">TOKEN</span><span className="token_text_two">0.00000001</span> </h3>
          {/* <h3 className='token_number'>0.00000001</h3> */}
        </div>


        <div className="send_receive">
          <div>
            <p className="send_text"><div className="send_text_item">SEND</div></p>
          </div>
          <div>
            <p className="receive_text"><div className="receive_text_item">RECEIVE</div></p>
          </div>

        </div>

        <div className="mid_sectionB">
          <div className="mid_sectionB_one">
            <p className="mid_sectionB_one_textB">Recent Activity</p>
          </div>
          <br />

        </div>

        <div className="table">

          <div className="">
            <h5 className="mid_sectionB_text">Date</h5>
            <h5 className="mid_sectionB_text">1/1/21</h5>
            <h5 className="mid_sectionB_text">2/2/2</h5>
            <h5 className="mid_sectionB_text">3/3/21</h5>

          </div>

          <div className="">
            <h5 className="mid_sectionB_text">Description</h5>
            <h5 className="mid_sectionB_text">Sent</h5>
            <h5 className="mid_sectionB_text">Received</h5>
            <h5 className="mid_sectionB_text">Sent</h5>

          </div>

          <div className="">
            <h5 className="mid_sectionB_text">Amount</h5>
            <h5 className="mid_sectionB_text">N100.00</h5>
            <h5 className="mid_sectionB_text">N200.00</h5>
            <h5 className="mid_sectionB_text">N300.00</h5>

          </div>

        </div>

        <br />



      </div>
    )
  }
}


export default MidSection